import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:csv/csv.dart';
import 'package:fl_chart/fl_chart.dart';

class InfluxDBLineGraph extends StatefulWidget {
  @override
  _InfluxDBLineGraphState createState() => _InfluxDBLineGraphState();
}

class _InfluxDBLineGraphState extends State<InfluxDBLineGraph> {
  String responseString = "initalizing ...";
  List<List<dynamic>> rowsAsListOfValues = [];
  @override
  void initState() {
    super.initState();
    getDataSync();
  }

  getDataSync() async {
    Response response = await post(
      "https://us-west-2-1.aws.cloud2.influxdata.com/api/v2/query?org=wojciech@kocjan.org",
      headers: {
        "Authorization":
            "Token mr5_PcjNsvf3yls-bWRfCsWBgPVbtb2tI-pymWjEfAkqgwf3Opi-ygCq0LQUKW76VOM4dXx4KuZHgh52svBtSQ==",
        "Accept": "application/csv",
        "Content-type": "application/vnd.flux",
      },
      body:
          "from (bucket: \"air-5m\") |> range(start: -2d, stop: -1d) |> yield()",
    );

    setState(() {
      CsvToListConverter converter = CsvToListConverter();
      rowsAsListOfValues = converter.convert(response.body);
      print(rowsAsListOfValues);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (rowsAsListOfValues == []) {
      return Text("nada");
    } else {
      List<FlSpot> spots = [];
      double i = 1.0;
      rowsAsListOfValues.forEach((List<dynamic> row) {
        if (i != 1.0) {
          try {
            double y = double.parse(row[6].toString());
            spots.add(FlSpot(i, y));
          } catch (Exception) {}
        }
        i++;
      });
      LineChartData chartOptions = LineChartData(
        lineBarsData: [
          LineChartBarData(
              spots: spots,
              dotData: FlDotData(show: false),
              colors: [Colors.green],
              barWidth: 0.5,
              belowBarData: BarAreaData(show: true, colors: [
                Colors.green.withOpacity(.4),
              ])),
        ],
        gridData: FlGridData(
            drawVerticalGrid: true,
            verticalInterval: 100.0,
            horizontalInterval: 1.0),
        backgroundColor: Colors.black,
        titlesData: FlTitlesData(
          bottomTitles: SideTitles(showTitles: false),
        ),
      );

      return Container(
        constraints: BoxConstraints.expand(),
        child: LineChart(
          chartOptions,
        ),
      );
    }
  }
}
